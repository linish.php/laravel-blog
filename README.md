## Template Used
[Simple Work](https://templated.co/simplework)

## Check this if onDelete('cascade') not working
[Check Link](https://stackoverflow.com/questions/24897300/laravel-foreign-key-ondeletecascade-not-working)

Either use
> $table->engine = 'InnoDB';

Or update the table engine type in database.php as follows

` 'mysql' => [
    'driver' => 'mysql',
    'url' => env('DATABASE_URL'),
    'host' => env('DB_HOST', '127.0.0.1'),
    'port' => env('DB_PORT', '3306'),
    'database' => env('DB_DATABASE', 'forge'),
    'username' => env('DB_USERNAME', 'forge'),
    'password' => env('DB_PASSWORD', ''),
    'unix_socket' => env('DB_SOCKET', ''),
    'charset' => 'utf8mb4',
    'collation' => 'utf8mb4_unicode_ci',
    'prefix' => '',
    'prefix_indexes' => true,
    'strict' => true,
    'engine' => 'InnoDB',
    'options' => extension_loaded('pdo_mysql') ? array_filter([
        PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
    ]) : [],
]`

### To create symn link
> php artisan storage:link

### Tinker Commands
> php artisan tinker

### Example
> User::factory()->count(10)->create();


### Loop Example
> $users = User::all();
> 
> $users->each(function($user) { Tweet::factory()->count(10)->create(['user_id' => $user->id]); });

### Reference
[Laravel 8 Factory Tutorial](https://www.nicesnippets.com/blog/laravel-8-factory-example-tutorial)

## Another use case with onDelete relationship
For another use case if you have articles but you don't want to delete them from your site when the user that wrote them is deleted. Instead of `onDelete('cascade')` you can use `onDelete('set null')` which will remove the user_id from the article but still leave it in the database.

Then in your views when displaying the author you could check for the null condition and display something like deleted author.

[Reference](https://laracasts.com/series/laravel-6-from-scratch/episodes/30)

OR

or use this [Default Models](https://laravel.com/docs/6.x/eloquent-relationships#default-models)

```
public function author()
{
    return $this->belongsTo(User::class, 'author_id')->withDefault([
        'name' => 'Guest Author',
    ]);
}
```

## Convension of pivot table
* Take the tables say 2 (`articles` & `tags`)
* Take the singular name `aricle` & `tag`
* Order it alphabetically and separate it by underscore `article_tag`
  
## Example
```
Schema::create('tags', function (Blueprint $table) {
    $table->id();
    $table->string('name');
    $table->timestamps();
});
```

```
Schema::create('article_tag', function (Blueprint $table) {
    $table->id();
    $table->unsignedBigInteger('article_id');
    $table->unsignedBigInteger('tag_id');
    $table->timestamps();
    $table->unique(['article_id', 'tag_id']);
    $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
    $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
});
```